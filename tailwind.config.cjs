module.exports = {
	mode: 'jit',
	purge: ['./src/**/*.svelte'],
	variants: {
		extend: {
			transform: ['hover']
		}
	}
};
