import type { PokemonBaseInfos } from 'src/types/pokemon';
import { fetchWithCache } from 'src/utils/fetchUtils';
import { writable } from 'svelte/store';

export const pokemons = writable<PokemonBaseInfos[]>([]);

const fetchPokemons = () => {
	fetchWithCache(`https://pokeapi.co/api/v2/pokemon?limit=5000`).then((data) => {
		const pokemonData: PokemonBaseInfos[] = data.results
			.map((pokemon, index) => ({
				id: index + 1,
				name: pokemon.name
			}))
			.filter((pokemon) => pokemon.id < 899); // little hack cuz different types of objects after 899
		pokemons.set(pokemonData);
	});
};

fetchPokemons();
