export const fetchWithCache = async (url: string) => {
	const isLocalStorage = typeof localStorage !== 'undefined';
	const cachedValue = isLocalStorage ? localStorage.getItem(url) : undefined;
	if (cachedValue) {
		return Promise.resolve(JSON.parse(cachedValue));
	} else {
		const response = await fetch(url);
		const data = await response.json();
		if (isLocalStorage) localStorage.setItem(url, JSON.stringify(data));
		return data;
	}
};

export const fetchJson = async (url: string) => {
	const response = await fetch(url);
	return response.json();
};
