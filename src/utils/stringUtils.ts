export const capitalizeFirstLetter = (str?: string) => {
	if (!str) return '';
	return str.charAt(0).toUpperCase() + str.slice(1);
};

export const extractIdFromUrl = (url: string): number | undefined => {
	const urlParts = url.split('/');
	return urlParts.length >= 2 ? Number(urlParts[urlParts.length - 2]) : undefined;
};
