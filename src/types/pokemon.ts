export interface Pokemon {
	id: number;
	name: string;
	sprite: string;
	height: number;
	weight: number;
	types: string[];
}

export interface PokemonBaseInfos {
	id: number;
	name: string;
}

export enum EvolutionTrigger {
	LEVEL_UP = 'level-up',
	TRADE = 'trade',
	USE_ITEM = 'use-item'
}

export const TriggerLabel = {
	'level-up': 'Lvl',
	trade: 'Trade',
	'use-item': 'Item'
};

// TODO: smart type evolvesInto mandatory if evolves is present and vice-versa
export interface PokemonEvolutionChain extends PokemonBaseInfos {
	evolves?: EvolutionTrigger;
	evolvesAtLvl?: number;
	evolvesInto?: PokemonEvolutionChain;
}
